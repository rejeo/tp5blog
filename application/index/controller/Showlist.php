<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
class Showlist extends Controller
{
    public function index()
    {
        $cid = input("cid");
        if (!is_numeric($cid)) {
            $this->error('展示出错！');
        }
        $articleres = Db::name('Article')->where('cateid',$cid)->paginate(10);
        $cares = Db::name('Cate')->where('id',$cid)->find();
        $this->assign(array('articleres'=>$articleres,'cares'=>$cares));
        return $this->fetch('index/list');
    }
}
