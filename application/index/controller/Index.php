<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
class Index extends Controller
{
    public function index()
    {
        $articleres = Db::name('Article')->paginate(10);
        // 需要做分页
        // $articleres = Db::name('Article')->limit(10)->select();
        $this->assign('articleres',$articleres);
        return $this->fetch('index/index');// 自定义加载模版
        // return $this->error('报错了!','','',30);
    }
}
