<?php
namespace app\index\controller;

use think\Controller;
use think\Db;

class Article extends Controller
{
    public function index()
    {
        $arid  = input('aid');
        if (!is_numeric($arid)) {
           return $this->error('展示出错！');
        }
        $num   = Db::name('Article')->where('id',$arid)->setInc('click', 1, 3);// 自增延迟更新
        $arres = Db::name('Article')->where('id',$arid)->find();
        $cares = Db::name('Cate')->where('id',$arres['cateid'])->find();
        $reres = Db::name('Article')
                    ->where(array('cateid'=>$arres['cateid'],'state'=>1))
                    ->field('id,title,pic')
                    ->limit(8)
                    ->select();
        $this->assign(array('arres'=>$arres,'cares'=>$cares,'reres'=>$reres));
        return $this->fetch('index/article');// 自定义加载模版
    }

    // 相关阅读，就是对当前文章进行keywords分词的like查询
    // 在做相关信息建立的时候，需要对重复的数据进行去重，且当前以打开的页面
    // 也不用显示在这里面
}
