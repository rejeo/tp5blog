<?php
/**
 * 栏目生成，可做定时生成
 * Created by Sublime Text 3.
 * @authors Rejeo (rejeo@qq.com)
 * @date    2018-04-12 15:31:54
 * @version $Id$
 */
namespace app\index\controller;
use think\Controller;
use think\Db;
class Cate extends Controller
{
    // 信息展示模版
    private $fileshow = APP_PATH.'view/dispatch_show.htm';
    private $data = [
                'code'=>1,
                'msg'=>'',
            ];
    // 生成栏目公共页面
    public function index()
    {
        // 要生成的文件
        $filename = APP_PATH.'view/index/header.htm';
        // 栏目缓存
        $cateres = Db::name('cate')->cache('cate',360)->select();
        $this->assign('cateres',$cateres);
        // 解析模版
        $content = $this->fetch('index/header-tpl');
        if (!is_readable($filename)&&!is_writable($filename)) {
            $this->data['msg'] = '文件不可写！';
            return $this->fetch($this->fileshow,$this->data);
        }
        $flag = file_put_contents($filename, $content, LOCK_EX); // 加锁防止并发
        if (FALSE===$flag) {
            $this->data['msg'] = '写入数据失败！';
            return $this->fetch($this->fileshow,$this->data);

        }
        $this->data['msg'] = '栏目生成成功！';
        return $this->fetch($this->fileshow,$this->data);
    }

    // 生成热门点击，推荐阅读公共页面
    public function right()
    {
        // 要生成的文件
        $filename = APP_PATH.'view/index/right.htm';
        // 热门点击
        $clres = Db::name('Article')
                    ->where(1)
                    ->field('id,title')
                    ->order('click desc')
                    ->limit(4)
                    ->select();
        // 推荐推荐阅读
        $stres = Db::name('Article')
                    ->where('state',1)
                    ->field('id,title')
                    ->order('id desc')
                    ->limit(5)
                    ->select();
        $this->assign(array(
                'clres'=>$clres,
                'stres'=>$stres
            ));
        // 解析模版
        $content = $this->fetch('index/right-tpl');
        if (!is_readable($filename)&&!is_writable($filename)) {
            $this->data['msg'] = '文件不可写！';
            return $this->fetch($this->fileshow,$this->data);
        }
        $flag = file_put_contents($filename, $content, LOCK_EX);// 加锁防止并发
        if (FALSE===$flag) {
            $this->data['msg'] = '写入数据失败！';
            return $this->fetch($this->fileshow,$this->data);

        }
        $this->data['msg'] = '右侧展示生成成功！';
        return $this->fetch($this->fileshow,$this->data);
    }
}
