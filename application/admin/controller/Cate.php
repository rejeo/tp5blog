<?php
namespace app\admin\controller;
use think\Controller;
use app\admin\model\Cate as CateModel;
/**
 * 栏目管理
 */
class Cate extends Controller
{
    public function index()
    {
        $list = CateModel::paginate(3);// 分页输出，这个模版报错存在误差
        // $list = Db::name('admin')->paginate(3);// 这个模版报错比较明显，以后遇到不明报错就用这个
        $this->assign('list',$list);
        return $this->fetch('admin/catelist');// 自定义加载模版
    }
    // 添加栏目
    public function add()
    {
        if (request()->isPost()) {
            $data=[
                'catename'=>input('post.catename'),
            ];
            $validate = $this->validate($data,'Cate');// 实例化验证规则
            if(true !== $validate){
                return $this->error($validate);
            }
            $dblink = new CateModel();
            // $flag = $dblink->where('catename',$data['catename'])->find();
            // if (!is_null($flag)) {
            //     return $this->error('栏目已存在！');
            // }
            $flag = $dblink->create($data);
            if ($flag) {
              return $this->success('栏目添加成功！');
            }else{
              return $this->error('Data Error！');
            }
        }
        return $this->fetch('admin/cateadd');// 自定义加载模版
    }
    // 编辑栏目
    public function edit()
    {
        $id = input('id');// admin/index/edit/id/1.html 这样的方式不能用get获取id值
        // $id = 1;
        if (request()->isPost()) {
            $data = [
                'catename'=>input('post.catename'),
            ];
            $validate = $this->validate($data,'Cate');// 实例化验证规则和场景使用
            if(true !== $validate){
                return $this->error($validate);
            }else{
                $dblink = new CateModel();
                // $flag = $dblink->where('catename',$data['catename'])->find();
                // if (!is_null($flag)) {
                //     return $this->error('修改名栏目已存在！');
                // }
                $res = $dblink->where('id',$id)->update($data);
                if ($res) {
                    return $this->success('栏目修改成功！');
                }else{
                    return $this->error('栏目修改失败！');
                }
            }
        }elseif(is_numeric($id)){
            $dblink = new CateModel();
            $res = $dblink->where('id',$id)->find();
            if (!is_null($res)) {
                $this->assign('res',$res);
                return $this->fetch('admin/cateedit');
            }else{
                return $this->error('提交有误！');
            }
        }else{
            return $this->error('提交有误！');
        }
    }
    // 删除栏目
    public function del()
    {
        return $this->success('您没有删除权限！');
    }
}
