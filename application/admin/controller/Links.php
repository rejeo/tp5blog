<?php
namespace app\admin\controller;
use think\Controller;
use app\admin\model\Links as LinksModel;
class Links extends Controller
{
    public function index()
    {
        $list = LinksModel::paginate(3);// 分页输出，这个模版报错存在误差
        // $list = Db::name('admin')->paginate(3);// 这个模版报错比较明显，以后遇到不明报错就用这个
        $this->assign('list',$list);
        return $this->fetch('admin/linklist');// 自定义加载模版
    }
    // 添加友链
    public function add()
    {
        if (request()->isPost()) {
            $data=[
                'title'=>input('post.title'),
                'url'=>input('post.url'),
                'desc'=>input('post.desc'),
            ];
            $validate = $this->validate($data,'User.links');// 实例化验证规则
            if(true !== $validate){
                return $this->error($validate);
            }
            // return $this->success('管理员添加成功！'); // debug flag
            $links = new LinksModel();
            // $flag = $links->where('title',$data['title'])->find();
            // if (!is_null($flag)) {
            //     return $this->error('友链已存在！');
            // }
            // $flag = $links->validate('User.links')->create($data); // 在添加的同时使用验证
            $flag = $links->create($data);
            // $links->data($data);
            // $flag = $links->save();
            if ($flag) {
              return $this->success('管理员添加成功！');
            }else{
              return $this->error('Data Error！');
            }
        }
        return $this->fetch('admin/linkadd');// 自定义加载模版
    }
    // 编辑友链
    public function edit()
    {
        $id = input('id');// admin/index/edit/id/1.html 这样的方式不能用get获取id值
        // $id = 1;
        if (request()->isPost()) {
            $data = [
                'title'=>input('post.title'),
                'url'=>input('post.url'),
                'desc'=>input('post.desc'),
            ];
            $validate = $this->validate($data,'User.links');// 实例化验证规则和场景使用
            if(true !== $validate){
                return $this->error($validate);
            }else{
                $links = new LinksModel();
                // $flag = $links->where('title',$data['title'])->find();
                // if (!is_null($flag)) {
                //     return $this->error('友链已存在！');
                // }
                $res = $links->where('id',$id)->update($data);
                if ($res) {
                    return $this->success('友链修改成功！');
                }else{
                    return $this->error('友链修改失败！');
                }
            }
        }elseif(is_numeric($id)){
            $links = new LinksModel();
            $res = $links->where('id',$id)->find();
            if (!is_null($res)) {
                $this->assign('res',$res);
                return $this->fetch('admin/linkedit');
            }else{
                return $this->error('提交有误！');
            }
        }else{
            return $this->error('提交有误！');
        }
    }
    // 删除友链
    public function del()
    {
        return $this->success('您没有删除权限！');
    }
}
