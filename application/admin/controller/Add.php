<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;

// 用户添加，可以整合为一个文件，我这是单独拿出来了
class Add extends Controller
{
    public function index()
    {
        if (request()->isPost()) {
            $data=[
                'username'=>input('post.username'),
                'password'=>input('post.password'),
            ];
            $validate = $this->validate($data,'User.add');// 实例化验证规则
            if(TRUE !== $validate){
                $this->error($validate);
            }
            // 可以通过配置验证信息来判断姓名是否重合
            // $flag = Db::name('admin')->where('username',$data['username'])->find();
            // if (!is_null($flag)) {
            //     $this->error('管理员已存在！');
            // }
            $pass = encrypt_pw($data['password']);
            $data['password']=$pass;
            $flag = Db::name('admin')->insert($data);
            if ($flag) {
                $this->success('管理员添加成功！');
            }else{
                $this->error('Data Error！');
            }
        }
        return $this->fetch('admin/add');// 自定义加载模版
        // return 'hello world!';
    }
}
