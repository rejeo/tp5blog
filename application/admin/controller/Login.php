<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
// 登录管理
class Login extends Controller
{
    public function index()
    {
        if (session('?username')) {
            $this->success("登录成功！",'admin/index/index');
        }
        if (request()->isPost()) {
            if(!captcha_check(input('post.captcha'))){
                $this->error('验证码有误！'); //验证失败
            }
            $data = [
                'username'=>input('post.username'),
                'password'=>input('post.password'),
            ];
            $validate = $this->validate($data,'User.login');// 实例化验证规则和场景使用
            if(TRUE !== $validate){
                $this->error($validate);
            }
            $pass = encrypt_pw($data['password']);
            $passwd = Db::name('admin')->field('password')->where('username','=',$data['username'])->find();
            if ($pass===$passwd['password']) {
              session('username',$data['username']);
                $this->success('登录成功！','admin/index/index');
            }else{
                $this->error('登录密码有误！');
            }
        }
        return $this->fetch('admin/login');// 自定义加载模版
    }
    public function loginout()
    {
        session('null');
        return $this->fetch('admin/login');
    }
}
