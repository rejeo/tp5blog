<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
// 后台管理首页
class Index extends Controller
{
    public function index()
    {
        // if (!session('?username')) {
        //     return $this->error("请登录！");
        // }
        return $this->fetch('admin/index');// 自定义加载模版
        // return 'hello world!';
    }
    // 编辑用户
    public function edit()
    {
        if (!session('?username')) {
            return $this->error("请登录！",'admin/login/index');
        }
        $id = input('id');// admin/index/edit/id/1.html 这样的方式不能用get获取id值
        // $id = 1;
        if (request()->isPost()) {
            $data = [
                'password'=>input('post.password'),
            ];
            $validate = $this->validate($data,'User.edit');// 实例化验证规则和场景使用
            if(true !== $validate){
                return $this->error($validate);
            }else{
                $data['password'] = encrypt_pw($data['password']);
                $res = Db::name('admin')->where('id',$id)->update(['password' => $data['password']]);
                if ($res) {
                    return $this->success('密码修改成功！');
                }else{
                    return $this->error('密码修改失败！');
                }
            }
        }elseif(is_numeric($id)){
            $res = Db::name('admin')->field('id,username')->where('id',$id)->find();
            if (!is_null($res)) {
                $this->assign('res',$res);
                return $this->fetch('admin/useredit');
            }else{
                return $this->error('提交有误！');
            }
        }else{
            return $this->error('提交有误！');
        }
    }
    // 删除用户
    public function del()
    {
        // $adminid = config('AdminIdArray');// 获取管理员ID组
        // $id = input('get.id');
        // if(is_numeric($id)){
        //     if(in_array($id,$adminid)){
        //          return $this->error('不能删除管理员！');
        //     }
        //     $res = Db::name('admin')->where('id','=',$id)->delect();
        //     if (!is_null($res)) {
        //         return $this->success('删除成功！');
        //     }else{
        //         return $this->error('提交有误！');
        //     }
        // }else{
        //     return $this->error('提交有误！');
        // }
        return $this->success('您没有删除权限！');
    }

}
