<?php
/**
 * 基本类
 * Created by Sublime Text 3.
 * @authors Rejeo (rejeo@qq.com)
 * @date    2018-04-08 10:18:21
 * @version $Id$
 */
namespace app\admin\controller;
use think\Controller;
// 后台管理首页
class Base extends Controller
{
    protected function _initialize()
    {
        if (!session('?username')) {
            return $this->error("请登录！",'admin/login/index');
        }
    }
}
