<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\model\Article as ArticleModel;
/**
 * 文章管理
 */
class Article extends Controller
{
    public function index()
    {
        // $list = ArticleModel::paginate(10);// 分页输出，这个模版报错存在误差
        $fields = 'a.id, `title`, `author`, `desc`, `keywords`, `content`,
                   `pic`, `click`, `state`, `time`, `catename`';
        $list = Db::name('Article')->alias('a')->join('cate c','a.cateid=c.id')->field($fields)->paginate(10);// 比较关联模型，还是这个用得比较明了
        // $list = Db::name('Article')->paginate(3);// 这个模版报错比较明显，以后遇到不明报错就用这个
        $this->assign('list',$list);
        return $this->fetch('admin/articlelist');// 自定义加载模版
    }
    // 添加文章
    public function add()
    {
        if (request()->isPost()) {
            $data = [
                "title"    =>input('post.title'),
                "author"   =>input('post.author'),
                "keywords" =>str_replace('，',',',input('post.keywords')),// 将中文逗号替换为英文逗号
                "desc"     =>input('post.desc'),
                "cateid"   =>input('post.cateid'),
                "state"    =>input('post.state'),
                "content"  =>input('post.content'),
                "time"     =>time(),
            ];

            $msg = $this->uploadFile($_FILES);
            if(1===$msg['code']){
                $data['pic'] = $msg['msg'];
            }elseif(4===$msg['code']) {
                $data['pic'] = '';
            }else{
                $this->error($msg['msg']);
            }
            if ('on'==$data['state']) {
                $data['state'] = 1;
            }else{
                $data['state'] = 0;
            }
            $validate = $this->validate($data,'Article');// 实例化验证规则
            if(TRUE !== $validate){
                return $this->error($validate);
            }
            $dblink = new ArticleModel();
            $flag = $dblink->create($data);
            if ($flag) {
              return $this->success('文章添加成功！','Article/index');
            }else{
              return $this->error('文章添加失败！');
            }
        }
        // 使用查询缓存，添加标识和时间
        $cateres = Db::name('cate')->cache('cate',360)->select();// 栏目缓存
        $this->assign('cateres',$cateres);
        return $this->fetch('admin/articleadd');// 自定义加载模版
    }
    // 编辑文章
    public function edit()
    {
        $id = input('id');
        if (request()->isPost()) {
            $data = [
                "title"    =>input('post.title'),
                "author"   =>input('post.author'),
                "keywords" =>str_replace('，',',',input('post.keywords')),
                "desc"     =>input('post.desc'),
                "cateid"   =>input('post.cateid'),
                "state"    =>input('post.state'),
                "content"  =>input('post.content'),
                "time"     =>time(),
            ];

            $msg = $this->uploadFile($_FILES);
            if(1===$msg['code']){
                $data['pic'] = $msg['msg'];
            }elseif(4===$msg['code']) {
                $data['pic'] = '';
            }else{
                $this->error($msg['msg']);
            }

            if ('on'==$data['state']) {
                $data['state'] = 1;
            }else{
                $data['state'] = 0;
            }

            $validate = $this->validate($data,'Article');// 实例化验证规则和场景使用
            if(TRUE !== $validate){
                return $this->error($validate);
            }

            $dblink = new ArticleModel();
            $res = $dblink->where('id',$id)->update($data);
            if ($res) {
                return $this->success('文章修改成功！');
            }else{
                return $this->error('文章修改失败！');
            }

        }elseif(is_numeric($id)){
            $dblink = new ArticleModel();
            $articles = $dblink->where('id',$id)->find();
            if (!is_null($articles)) {
                $cateres = Db::name('cate')->cache('cate',360)->select();// 栏目缓存
                $this->assign('cateres',$cateres);
                $this->assign('articles',$articles);
                return $this->fetch('admin/articleedit');
            }else{
                return $this->error('提交有误！');
            }
        }else{
            return $this->error('提交有误！');
        }
    }
    // 删除文章
    public function del()
    {
        return $this->success('您没有删除权限！');
    }

    /**
     * 图片上传
     * @param  array  $array 要上传的图片资源
     * @return mixed
     */
    private function uploadFile(array $array)
    {
        $msg = [
            'code' => 0,
            'msg'  => '',
            'data' => [],
        ];
        // 获取上传文件的名字等价于
        // array_keys($_FILES);// 当数组是二维数组，且第一层只有一个键的时候
        $filename = key($array);
        if(!empty($array[$filename]['tmp_name'])&&0===$array[$filename]['error'])
        {
            // 不能在前端设置了MAX_FILE_SIZE这个隐藏属性时及时报错
            // 甚至当使用validate(['size'=>15678,'ext'=>'jpg,png,gif'])，验证都不行
            // 因为通过request()->file('imgname')获取到的值直接就是null
            $file = request()->file($filename);
            $savepath = ROOT_PATH . 'public' . DS . 'uploads';
            $info = $file->move($savepath);
            if($info){
                // 为了方便迁移只存取图片所在位置
                $msg['code'] = 1;
                $msg['msg'] = $info->getSaveName();
                return $msg;
            }else{
                // 上传失败获取错误信息
                $msg['msg'] = $file->getError();
                return $msg;
            }
        }elseif(4===$array[$filename]['error']) {
            $msg['code'] = 4;
            return $msg;
        }else{
            // php中$_FILES['filename']['error']上传文件失败对应的错误代码和解释
            // 其值为 0，没有错误发生，文件上传成功。
            // 其值为 1，上传的文件超过了php配置中 upload_max_filesize 选项限制的值。
            // 其值为 2，上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值。
            // 其值为 3，文件只有部分被上传。
            // 其值为 4，没有文件被上传。
            // 其值为 6，找不到临时文件夹。
            // 其值为 7，文件写入失败。
            switch ($array[$filename]['error']) {
                case '1':
                    $errstr = '上传的文件超过了php配置中 upload_max_filesize 选项限制的值。';
                    break;
                case '2':
                    $errstr = '上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值。';
                    break;
                case '3':
                    $errstr = '文件只有部分被上传。';
                    break;
                case '6':
                    $errstr = '找不到临时文件夹。';
                    break;
                case '7':
                    $errstr = '文件写入失败。';
                    break;
                default:
                    $errstr = '上传文件发生未知错误！';
                    break;
            }
            $msg['msg'] = $errstr;
            return $msg;
        }
    }
}
