<?php
namespace app\admin\controller;
use think\Controller;
use app\admin\model\User as UserModel;
// use think\Db;
// 用户列表，不应该单独拿出来的，可以整合
class Show extends Controller
{
    public function index()
    {
        $list = UserModel::paginate(3);// 分页输出，这个模版报错存在误差
      // $list = Db::name('admin')->paginate(3);// 这个模版报错比较明显，以后遇到不明报错就用这个
        $this->assign('list',$list);
        return $this->fetch('admin/list');// 自定义加载模版
        // return 'hello world!';
    }
}
