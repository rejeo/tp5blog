<?php
namespace app\admin\model;
use think\Model;
// 文章模型
class Article extends Model
{
    // 定义关联表，关联tp_cate栏目表，需要建立对应的数据模型文件
    // 定义相对的关联，没能理解这个调用的原理，和整个使用原理，只能使用罢了
    public function cate()
    {
        return $this->belongsTo('cate','cateid');
    }
}
