<?php
namespace app\admin\validate;
use think\Validate;
// 验证规则配置
class User extends Validate
{
    /*验证规则，注意多去使用一些内置规则*/
    protected $rule = [
        'id' => 'number',
        'username' => 'require|max:20',
        'password' => 'require|max:20',
        'title' => 'require|max:30',
        'url' => 'require|url|max:255',
        'desc' => 'require|max:255',
    ];
    /*验证提示*/
    protected $message = [
        // 'id.number' => 'id必须是数字',
        'username.unique' => '管理员已存在！',
        // 'username.require' => 'Username Or PassWord Is Null!',
        // 'username.max' => 'Username Or PassWord is too long!Must be less than 20!',
        // 'password.require' => 'Username Or PassWord Is Null!',
        // 'password.max' => 'Username Or PassWord is too long!Must be less than 20!',
        'title.unique' => '友链已存在！',

    ];
    /*场景验证*/
    /*表示验证edit场景（该场景定义只需要验证name和age字段）*/
    protected $scene = [
        'login' => ['username','password'],
        'add' => ['username'=>'require|max:20|unique:admin','password'],
        'edit' => ['id','password'],
        'links' => ['title'=>'require|max:30|unique:links','url','desc'],
    ];
}
