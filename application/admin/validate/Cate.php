<?php
/**
 * 栏目验证规则配置
 * Created by Sublime Text 3.
 * @authors Rejeo (rejeo@qq.com)
 * @date    2018-04-08 10:43:33
 * @version $Id$
 */
namespace app\admin\validate;
use think\Validate;
// 验证规则配置
class Cate extends Validate {

    /*验证规则，注意多去使用一些内置规则*/
    protected $rule = [
        'catename' => 'require|max:30|unique:cate',
    ];
    /*验证提示*/
    protected $message = [
        'catename.unique' => '栏目存在！',

    ];
}
