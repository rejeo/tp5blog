<?php
/**
 * 文章验证规则配置
 * Created by Sublime Text 3.
 * @authors Rejeo (rejeo@qq.com)
 * @date    2018-04-08 10:43:33
 * @version $Id$
 */
namespace app\admin\validate;
use think\Validate;
// 验证规则配置
class Article extends Validate {

    // /*验证规则，注意多去使用一些内置规则*/
    protected $rule = [
        'title'  => 'require|max:30|unique:article',
        'cateid' => 'number',
    ];
    // /*验证提示*/
    // protected $message = [
    //     'catename.unique' => '栏目存在！',

    // ];
}
