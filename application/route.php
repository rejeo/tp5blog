<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    // 在 __pattern__ 中定义的变量规则我们称之为全局变量规则，
    // 在路由规则里面定义的变量规则我们称之为局部变量规则，
    // 如果一个变量同时定义了全局规则和局部规则的话，当前的局部规则会覆盖全局规则的
    '__pattern__' => [
        'name' => '\w+',
    ],
    // 以下全部定义了局部变量规则
    // 路由分组
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],
    // 'article' => 'index/article/index',
    // 'cate' => 'index/cate/index',
    // 'showlist' => 'index/showlist/index',
    // 我们希望使用 Cate 可以访问index模块的Cate控制器的所有操作
    // '__alias__' => [
    //     'article' => 'index/article',
    //     'cate' => 'index/cate',
    //     'showlist' => 'index/showlist',
    // ],
    // 没有匹配到的路由默认转到下面的路由去
    // '__miss__' => 'public/miss',
];
