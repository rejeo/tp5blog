<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
if (!function_exists('encrypt_pw')) {
    /**
     * 密码加密
     * @param  string $pw 输入的密码
     * @param  string $en 安全码
     * @return string     加密后的字符串
     */
    function encrypt_pw($pw, $en='9BRvFQnt')
    {
        $enpw =md5(strrev($en.$pw));
        return $enpw;
    }
}
