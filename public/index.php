<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
// define('SITE_URL', 'http://localhost/tp5blog/public');// 本地直接使用
define('SITE_URL', 'http://www.tp5blog.com/');// 配置了apache的虚拟主机
// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
